import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="container mt-5 mb-5" style="max-width: 800px">
      <app-absence-registration></app-absence-registration>
      <br/>
      <app-absence-list></app-absence-list>
    </div>
  `
})
export class AppComponent {}
