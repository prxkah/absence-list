import { Component, OnInit } from '@angular/core';
import { AbsenceListService, AbsenceListQuery } from '../../state/absence-list/absence-list.service';

@Component({
  selector: 'app-absence-list',
  templateUrl: './absence-list.component.html'
})
export class AbsenceListComponent implements OnInit {
  items$ = this.absenceListQuery.selectItems();
  itemCount$ = this.absenceListQuery.selectCount();
  activeItemId$ = this.absenceListQuery.selectActiveId();

  constructor(private absenceListService: AbsenceListService, private absenceListQuery: AbsenceListQuery) {}

  ngOnInit() {
    this.absenceListService.list();
  }

  onShowMoreClick() {
    this.absenceListService.list();
  }

  onEditClick(id: number) {
    this.absenceListService.setActiveItem(id);
  }
}
