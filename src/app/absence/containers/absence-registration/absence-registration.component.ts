import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AbsenceListService, AbsenceListQuery, AbsenceListItem } from '../../state/absence-list/absence-list.service';
import { Observable } from 'rxjs';

const initialFormValues = {
  dateFrom: null,
  dateTo: null
};

@Component({
  selector: 'app-absence-registration',
  templateUrl: './absence-registration.component.html'
})
export class AbsenceRegistrationComponent {
  form: FormGroup = this.fb.group(initialFormValues);
  activeItem$ = this.absenceListQuery.selectActive();

  constructor(private fb: FormBuilder, private absenceListService: AbsenceListService, private absenceListQuery: AbsenceListQuery) {
    (<Observable<AbsenceListItem>>this.absenceListQuery.selectActive()).subscribe(item => {
      this.form.patchValue(item ? { ...item } : initialFormValues);
    });
  }

  onSaveClick() {
    this.absenceListService.upsert(this.form.value).subscribe({
      complete: () => this.form.reset()
    });
  }

  onRemoveClick(id: number) {
    this.absenceListService.delete(id).subscribe({
      complete: () => this.form.reset()
    });
  }

  onCancelClick(id: number) {
    this.absenceListService.setActiveItem(null);
  }
}
