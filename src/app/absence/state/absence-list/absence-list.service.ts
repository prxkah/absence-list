import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig, QueryEntity, QueryConfig, Order, transaction } from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { orderBy } from 'lodash';
import { map, tap } from 'rxjs/operators';
import { combineLatest, EMPTY } from 'rxjs';

const TAKE_AMOUNT = 5;

/* |------------- MODEL -------------| */

export interface AbsenceListItem {
  id: number;
  dateFrom: string;
  dateTo: string;
}

/* |------------- STORE -------------| */

interface State extends EntityState<AbsenceListItem> {
  takeAmount: number;
}

const initialState: State = {
  takeAmount: 0
};

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'absenceList' })
export class AbsenceListStore extends EntityStore<State, AbsenceListItem> {
  constructor() {
    super(initialState);
  }
}

/* |------------- QUERY -------------| */

@Injectable({ providedIn: 'root' })
@QueryConfig({ sortBy: 'dateFrom', sortByOrder: Order.DESC })
export class AbsenceListQuery extends QueryEntity<State, AbsenceListItem> {
  constructor(protected store: AbsenceListStore) {
    super(store);
  }

  selectItems() {
    return combineLatest(this.select(state => state.takeAmount), this.selectAll()).pipe(
      map(([takeAmount, items]) => {
        return items.slice(0, takeAmount);
      })
    );
  }
}

/* |------------- DATA-SERVICE -------------| */

@Injectable({ providedIn: 'root' })
export class AbsenceListDataService {
  constructor(private http: HttpClient) {}

  list(params: { index: number }) {
    return this.http.get<AbsenceListItem[]>('https://localhost:44313/api/absencelist/' + params.index);
  }

  add(item: AbsenceListItem) {
    return this.http.post('https://localhost:44313/api/absencelist', item);
  }

  update(item: AbsenceListItem) {
    return this.http.put('https://localhost:44313/api/absencelist', item);
  }

  delete(id: number) {
    return this.http.delete('https://localhost:44313/api/absencelist/' + id);
  }
}

/* |------------- SERVICE -------------| */

@Injectable({ providedIn: 'root' })
export class AbsenceListService {
  constructor(private store: AbsenceListStore, private query: AbsenceListQuery, private dataService: AbsenceListDataService) {}

  @transaction()
  upsert(item: AbsenceListItem) {
    const activeItem = this.query.getActive() as AbsenceListItem;
    const request = activeItem ? this.dataService.update({ id: activeItem.id, ...item }) : this.dataService.add(item);

    return request.pipe(
      tap(() => {
        const index = this.getIndex(item.dateFrom);
        const rowCount = this.query.getCount();

        if (index < rowCount) {
          this.dataService.list({ index }).subscribe(items => {
            this.store.upsert(items[0].id, { ...items[0] });
          });
        }
      })
    );
  }

  delete(id: number) {
    return this.dataService.delete(id).pipe(tap(() => this.store.remove(id)));
  }

  setActiveItem(id: number) {
    this.store.setActive(id);
  }

  @transaction()
  list() {
    this.dataService.list({ index: this.query.getCount() - 1 }).subscribe(items => {
      this.store.add(items);

      const takeAmount = this.query.getValue().takeAmount + TAKE_AMOUNT;
      this.store.update({ takeAmount });
    });
  }

  private getIndex(date: string) {
    const dates = this.query.getAll().map(item => item.dateFrom);
    dates.push(date);

    const sortedDates = orderBy(dates, null, 'desc');
    const index = sortedDates.indexOf(date);

    return index;
  }
}
