import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AbsenceRegistrationComponent } from './absence/containers/absence-registration/absence-registration.component';
import { AbsenceListComponent } from './absence/containers/absence-list/absence-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AbsenceRegistrationComponent,
    AbsenceListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
